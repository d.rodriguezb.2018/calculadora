# Programa Calculadora

# Funcion suma de dos números
def sumar(primero, segundo):
    suma = primero + segundo
    return suma


# Funcion resta de dos números
def restar(primero, segundo):
    resta = primero - segundo
    return resta


#Sumar y restar:
print("Suma de 1 + 2, resultado:", sumar(1, 2))

print("Suma de 3 + 4, resultado:", sumar(3, 4))

print("Resta de 6 - 5, resultado:", restar(6, 5))

print("Resta de 8 - 7, resultado:", restar(8, 7))
